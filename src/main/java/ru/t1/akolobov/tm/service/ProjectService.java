package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.IProjectRepository;
import ru.t1.akolobov.tm.api.IProjectService;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(Project project) {
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return add(new Project(name));
        return add(new Project(name, description));
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}
