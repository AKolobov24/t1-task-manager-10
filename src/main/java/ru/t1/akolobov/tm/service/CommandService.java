package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.ICommandRepository;
import ru.t1.akolobov.tm.api.ICommandService;
import ru.t1.akolobov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
