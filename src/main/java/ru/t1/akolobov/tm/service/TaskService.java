package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.ITaskRepository;
import ru.t1.akolobov.tm.api.ITaskService;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(Task task) {
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return add(new Task(name));
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
