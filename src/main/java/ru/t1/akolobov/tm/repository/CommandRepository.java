package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.ICommandRepository;
import ru.t1.akolobov.tm.model.Command;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            CMD_ABOUT,
            ARG_ABOUT,
            "Display developer info.");

    private static final Command VERSION = new Command(
            CMD_VERSION,
            ARG_VERSION,
            "Display application version.");

    private static final Command HELP = new Command(
            CMD_HELP,
            ARG_HELP,
            "Display list of terminal commands.");

    private static final Command INFO = new Command(
            CMD_INFO,
            ARG_INFO,
            "Display system resources information.");

    private static final Command ARGUMENTS = new Command(
            CMD_ARGUMENTS,
            ARG_ARGUMENTS,
            "Display available arguments to run application.");

    private static final Command COMMANDS = new Command(
            CMD_COMMANDS,
            ARG_COMMANDS,
            "Display available application commands.");

    private static final Command EXIT = new Command(
            CMD_EXIT,
            null,
            "Exit application.");

    private static final Command PROJECT_CLEAR = new Command(
            CMD_PROJECT_CLEAR,
            null,
            "Delete all projects.");

    private static final Command PROJECT_LIST = new Command(
            CMD_PROJECT_LIST,
            null,
            "Display list of all projects.");

    private static final Command PROJECT_CREATE = new Command(
            CMD_PROJECT_CREATE,
            null,
            "Create new project.");

    private static final Command TASK_CLEAR = new Command(
            CMD_TASK_CLEAR,
            null,
            "Delete all tasks.");

    private static final Command TASK_LIST = new Command(
            CMD_TASK_LIST,
            null,
            "Display list of all tasks.");

    private static final Command TASK_CREATE = new Command(
            CMD_TASK_CREATE,
            null,
            "Create new task.");

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, INFO,
            ARGUMENTS, COMMANDS,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
