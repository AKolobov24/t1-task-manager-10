package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.ITaskRepository;
import ru.t1.akolobov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

}
