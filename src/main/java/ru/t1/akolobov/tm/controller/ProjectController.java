package ru.t1.akolobov.tm.controller;

import ru.t1.akolobov.tm.api.IProjectController;
import ru.t1.akolobov.tm.api.IProjectService;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void displayProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projectList = projectService.findAll();
        int index = 1;
        for (final Project project : projectList) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

}
