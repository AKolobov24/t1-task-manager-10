package ru.t1.akolobov.tm.api;

import ru.t1.akolobov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
