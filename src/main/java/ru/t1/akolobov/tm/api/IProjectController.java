package ru.t1.akolobov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void displayProjects();

}
