package ru.t1.akolobov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void displayTasks();

}
